################################################################################
# Adapted from "Simulating a real solar system with 70 lines of Python code" by 
# ChongChong He on medium.com
# 
# 
################################################################################

#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from astropy.time import Time
from astroquery.jplhorizons import Horizons
from datetime import timedelta
from datetime import datetime
import argparse



# Planets, asteroids and spacecrafts to be printed
# The asteroids are those visited by Lucy (no data for satellites Queta and
# Menoetisu). 
# Asteroids have a label direction, that allows deciding where to put the label
# (if it is shown: see --asteroids-labels option).
# See JPL NASA Horizons tool: The NASA ID can be found in the generated
# ephemeris of each object.
spaceObjects = [ 
        {"name": "Sun", "nasaid": 10, "color": "yellow", "size": 2,
            "type": "sun"}, 
        {"name": "Mercury", "nasaid": 199, "color": "#6C8D9D", "size": 0.38,
            "type": "planet"}, 
        {"name": "Venus", "nasaid": 299, "color": "#467084", "size": 0.95,
            "type": "planet"}, 
        {"name": "Earth", "nasaid": 399, "color": "#27556C", "size": 1,
            "type": "planet"}, 
        {"name": "Mars", "nasaid": 499, "color": "#113D53", "size": 0.53,
            "type": "planet"}, 
        {"name": "Jupiter", "nasaid": 599, "color": "#03283A", "size": 11.2,
            "type": "planet"},
        {"name": "Lucy", "nasaid": -49, "color": "orange", "size": 0.01,
            "type": "spacecraft"},
        {"name": "Polymele", "nasaid": 15094, "color": "#DA6A6A", "size": 0.1,
            "type": "asteroid", "label_dir": -1},
        {"name": "Eurybates", "nasaid": 3548, "color": "#DA6A6A", "size": 0.1,
            "type": "asteroid", "label_dir": 1},
        {"name": "Leucus", "nasaid": 11351, "color": "#DA6A6A", "size": 0.1,
            "type": "asteroid", "label_dir": 1},
        {"name": "Patroclus", "nasaid": 617, "color": "#DA6A6A", "size": 0.1,
            "type": "asteroid", "label_dir": 1},
        {"name": "Orus", "nasaid": 21900, "color": "#DA6A6A", "size": 0.1,
            "type": "asteroid", "label_dir": 1},
        {"name": "Donaldjohanson", "nasaid": 52246, "color": "#AA5585",
            "size": 0.1, "type": "asteroid", "label_dir": -1}
    ]



# Planets and other objects definition
class Object:                   
    #def __init__(self, name, nasaid, rad, color, pos_x, pos_y, tail, obj_type, 
    #        ax, args):
    def __init__(self, obj, factor, pos_x, pos_y, tail, ax, args):
        self.name = obj["name"]
        self.nasaid = obj["nasaid"]
        self.type = obj["type"]
        self.size=obj["size"]*factor
        self.color = obj["color"]
        self.plot = None
        self.label = None

        if "label_dir" in obj.keys(): 
            self.label_dir = obj["label_dir"] 
        
        # Current position
        self.current_x = None
        self.current_y = None

        # Positions: x and y vectors from Horizons ephemerids
        self.pos_x = pos_x
        self.pos_y = pos_y
        
        # Trace trajectory or not
        self.tail = tail
      
        # For asteroids, add a label with the name (if corresponding argument 
        # --asteroids-labels is set.
        # The size of the object is proportional to the number of letters in its
        # name.
        if self.type == "asteroid" and args.asteroids_labels: 
            label_size_ratio = (len(self.name)**2)*30
            self.label = ax.scatter(self.pos_x[0], self.pos_y[0], 
                    color=self.color,s=self.size*label_size_ratio, 
                    edgecolors=None, zorder=10, marker='$'+self.name+'$')
        
        self.plot = ax.scatter(self.pos_x[0], self.pos_y[0], color=self.color,
            s=self.size**2, edgecolors=None, zorder=10)

        if self.type == "asteroid":
            self.line, = ax.plot([], [], color=self.color, linewidth=0.5)
        else:
            self.line, = ax.plot([], [], color=self.color, linewidth=0.5,
                    label=self.name)



# Solar system definition
class SolarSystem:
    def __init__(self, thesun, ax, args):
        self.thesun = thesun
        self.planets = []
        self.timestart = None
        self.timestop = None
        self.current_time = 0
        self.timestamp = ax.text(.03, .94, 'Date: ', color='w', 
                transform=ax.transAxes, fontsize='x-large')

    def add_planet(self, planet):
        self.planets.append(planet)

    # evolve the trajectories
    def evolve(self, args):
        plots = []
        lines = []
        for p in self.planets:

            p.current_x = p.pos_x[self.current_time]
            p.current_y = p.pos_y[self.current_time]

            if p.label and args.asteroids_labels:
                offset_value = len(p.name)*0.1
                p.label.set_offsets([p.current_x+offset_value*p.label_dir, 
                    p.current_y+0.05])
                plots.append(p.label)

            p.plot.set_offsets([p.current_x, p.current_y])
            plots.append(p.plot)

            if p.tail == True:
                p.line.set_xdata(p.pos_x[0:self.current_time])
                p.line.set_ydata(p.pos_y[0:self.current_time])
                lines.append(p.line)
        
        # Convert current time (number of days since start date, since step=1d)
        date_printable = datetime.strptime(self.timestart, '%Y-%m-%d %H:%M') + \
            (timedelta(days=self.current_time))
        self.timestamp.set_text('Date: ' + str(date_printable)[:10])
        self.current_time += 1
        return plots + lines + [self.timestamp]


def animate(i, ss, args):
    return ss.evolve(args)


def main():
    # Parse user arguments
    parser = argparse.ArgumentParser(description='Lucy mission animation')
    parser.add_argument('--asteroids-labels', action='store_true', 
        help="Show asteroids names (might slow down the animation")
    args = parser.parse_args()
    asteroidsMarkers = args.asteroids_labels

    # Time parameters, required by JPL NASA horizons API
    sim_start_date = "2021-10-16 10:40"
    sim_end_date = "2033-04-01" 
    step = "1d"

    # Relatives sizes are accurate (but need to be adapted for the sake of
    # beauty (Lucy size approximate)
    factor = 8

    # Graph initialization
    plt.style.use('dark_background')
    fig = plt.figure(figsize=[26, 6])
    ax = plt.axes([0., 0., 1., 1.], xlim=(-7, 10), ylim=(-7, 7))
    ax.set_aspect('equal')
    ax.axis('off')

    # Solar system initialization
    ss = SolarSystem(Object(spaceObjects[0], factor, [0, 0, 0], [0, 0, 0], 
        False, ax, args), ax, args) 
    ss.timestart = sim_start_date
    ss.timestop = sim_end_date


    # For each planet in solar system, add in solar system. 
    for obj in spaceObjects: 
        # Planets and spacecrafts are considered Major Bodies in JPL NASA
        # database
        if obj["type"] == "planet" or obj["type"] == "spacecraft":
            eph = Horizons(id=obj["nasaid"], location="@sun", 
                    id_type="majorbody", epochs={"start":ss.timestart,
                        "stop":ss.timestop, "step":step}).vectors()
            ss.add_planet(Object(obj, factor, eph["x"], eph["y"], True, ax, args))

        # Asteroids are considered Small Bodies in JPL NASA database
        elif obj["type"] == "asteroid":
            eph = Horizons(id=obj["name"], location="@sun", id_type="smallbody", 
                epochs={'start':ss.timestart, 'stop':ss.timestop, 
                    'step':step}).vectors()
            ss.add_planet(Object(obj, factor, eph["x"], eph["y"], False, ax, args))

    
    # Render animation
    plt.legend(loc="center right")
    ani = animation.FuncAnimation(fig, animate, fargs=(ss,args,), repeat=False,
            frames=365*22, blit=True, interval=20,)
    plt.show()


if __name__ == "__main__":
    main()


