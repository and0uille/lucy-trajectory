# Introduction

This script was developed to illustrate Lucy's travel through space. You'll 
find a commented version in the Moutmout videos available on Youtube and 
[Peertube](https://tube.chaouane.xyz/w/pQL3rzRr79fcCTE6C5eMSf).

Lucy is a probe that will visit Jupiter Trojans asteroids.

# How does it work?

The trajectory of each planet and asteroid in the animation is computed using
the NASA JPL ephemerides available
[here](https://ssd.jpl.nasa.gov/horizons/app.html#/). 


# Usage

You first need to install numpy, matplotlib, astropy and astroquery. Then the 
animation can be launched this way: 

```
python lucy-trajectory.py
```

Optionnally, the asteroids names can be printed next to the asteroids. This can
be resource consuming:

```
python lucy-trajectory.py --asteroids-labels
```

# Preview

Here is a short preview of the produced animation (without asteroids labels):

![Preview](preview.gif)


# Credits

This script was originally inspired by this [blog post](https://medium.com/analytics-vidhya/simulating-the-solar-system-with-under-100-lines-of-python-code-5c53b3039fc6) by [ChongChong He](https://medium.com/@chongchonghe) which represents the 
solar system using keplerian orbits. 
